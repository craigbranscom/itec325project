CREATE TABLE Bet_Users (username VARCHAR(32), password VARCHAR(16), balance INT(6))

CREATE TABLE Bet_Admins (username VARCHAR(32), password VARCHAR(16))

CREATE TABLE Bet_Events (name VARCHAR(32), complete TINYINT))

CREATE TABLE Bet_325A_Event (username VARCHAR(32), bet TINYINT, quantity INT(6), units VARCHAR(16), time TIMESTAMP DEFAULT NOW())